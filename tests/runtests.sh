#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
	rlRun "cp test.pbm $tmp/"
        rlRun "pushd $tmp"
        rlRun "set -o pipefail"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "potrace --help" 0 "Check help message"
        rlRun "potrace --version" 0 "Check version message"
        rlRun "potrace --license" 0 "Check license message"
        rlRun "potrace test.pbm" 0 "Check basic functionality"
        rlRun "potrace test.pbm -b svg" 0 "Check svg"
        rlRun "potrace test.pbm -b pdf" 0 "Check pdf"
        rlRun "potrace test.pbm -b eps" 0 "Check eps"
        rlRun "potrace test.pbm -b pgm" 0 "Check pgm"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd

